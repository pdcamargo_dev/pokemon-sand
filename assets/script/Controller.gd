extends Node
# This is script is loaded no matter what scene is the current one
func _ready():

	pass

# Function that returns a number from up 0 to 100
func random():
	randomize()
	var x = randi() % 101
	return x

func pause():
	get_tree().set_pause(true)
func unpause():
	get_tree().set_pause(false)
func quit_game():
	get_tree().quit()
