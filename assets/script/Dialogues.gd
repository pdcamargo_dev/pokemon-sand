extends Patch9Frame


var printing = false

var done_printing = false

var pressed = false

var timer = 0

var text_to_print = []

var current_char = 0

var current_text = 0

const SPEED = 0.1

onready var texts = get_node("Texts")

func _ready():
	set_fixed_process(true)
	set_process_unhandled_key_input(true)

func _unhandled_key_input(key_event):
	if key_event.is_action_pressed("ui_interact"):
		pressed = false
	elif key_event.is_action_released("ui_interact"):
		pressed = true

func _fixed_process(delta):
	if printing:
		if !done_printing:
			timer += delta
			if timer > SPEED:
				timer = 0
				texts.set_bbcode(texts.get_bbcode() + text_to_print[current_text][current_char])
				current_char += 1
			
			if current_char >= text_to_print[current_text].length():
				current_char = 0
				timer = 0
				done_printing = true
				current_text += 1
		elif pressed:
			done_printing = false
			texts.set_bbcode("")
			if current_text >= text_to_print.size():
				current_text = 0
				text_to_print = []
				printing = false
				set_hidden(true)
				get_node("/root/MainGame/Player").can_move = true

	pressed = false
	

func _print_dialogue(text):
	printing = true
	text_to_print = text
	get_node("/root/MainGame/Player").can_move = false