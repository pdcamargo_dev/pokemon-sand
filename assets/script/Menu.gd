extends Patch9Frame
# Variable that controls the menu
var menu = false
# Variable that says if menu is open
var open = false
# Variable that controls up arrow pressed
var arrow_up = false
# Variable that constrols down arrow pressed
var arrow_down = false
# Variable that keeps current menu label (array position)
var current_menu_label = 0
# Variable that keeps the array of menu labels
onready var labels = get_node("Labels").get_children()
# Variable that keeps the arrow sprite in menu
onready var pointer = get_node("Arrow")

onready var PlayerControl = get_node("/root/MainGame/Player")

func _ready():
	# Allows to use Input functions
	set_process_unhandled_key_input(true)
	# Allows to use delta (time) functions 
	set_fixed_process(true)
	# Calls the functions that updates the position of the pointer arrow when menu is open
	pointer_update()

func _menu_trigger():
	if current_menu_label == 0:
		# Calls the options below when the player interacts with "Pokedex Menu"
		_close_menu(false)
	elif current_menu_label == 1:
		# Calls the options below when the player interacts with "Pokemon Menu"
		open = false
	elif current_menu_label == 2:
		# Calls the options below when the player interacts with "Bag Menu"
		open = false
	elif current_menu_label == 3:
		# Calls the options below when the player interacts with "Player Menu"
		open = false
	elif current_menu_label == 4:
		# Calls the options below when the player interacts with "Save Menu"
		open = false
	elif current_menu_label == 5:
		# Calls the options below when the player interacts with "Option Menu"
		open = false
	elif current_menu_label == 6:
		# Calls the options below when the player interacts with "Quit Menu"
		open = false
		Controller.quit_game()

func _fixed_process(delta):
	if open:
		# If the interact button has been clicked when menu is open, calls a function to trigger the current label of menu
		if Input.is_action_pressed("ui_interact"):
			_menu_trigger()
		# If menu is true, closes the menu, and let player moves
		if menu:
			_close_menu(false)

		# If menu is open, and the arrow key up is clicked, changes the position of the pointer in menu
		if arrow_up:
			if current_menu_label == 0:
				current_menu_label = labels.size()-1
			else:
				current_menu_label -= 1
			pointer_update()
		# If menu is open, and the arrow key down is clicked, changes the position of the pointer in menu as well
		if arrow_down:
			if current_menu_label == labels.size()-1:
				current_menu_label = 0
			else:
				current_menu_label += 1
			pointer_update()

		menu = false
		arrow_up = false
		arrow_down = false

func _unhandled_key_input(key_event):
	if open:
		if key_event.is_action_pressed("ui_menu"):
			menu = true
		elif key_event.is_action_released("ui_menu"):
			menu = false
		if key_event.is_action_pressed("ui_down"):
			arrow_down = true
		elif key_event.is_action_released("ui_down"):
			arrow_down = false
		if key_event.is_action_pressed("ui_up"):
			arrow_up = true
		elif key_event.is_action_released("ui_up"):
			arrow_up = false
# Functions that updates the position of the pointer arrow in menu
func pointer_update():
	pointer.set_global_pos(Vector2(pointer.get_global_pos().x, labels[current_menu_label].get_global_pos().y+4))

func _open_menu():
	set_hidden(false)
	get_tree().set_pause(true)
	menu = false
	open = true
func _close_menu(option):
	if !option:
		set_hidden(true)
		Controller.unpause()
		open = false
	elif option == "keep_pause":
		set_hidden(true)
		open = false