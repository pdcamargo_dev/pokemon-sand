extends KinematicBody2D
# Var that says if player is trying to interact
var interact = false
# Var that says if menu is triggered
var menu = false
# Var that controls direction that the player will move
var direction = Vector2(0, 0)
# Var that keeps the start movement position of the player
var start_position = Vector2(0, 0)
# Var that says if player is moving or not
var moving = false
# Var that says if player can move or not
var can_move = true
# Constant that controls speed movement
const movement_speed = 1
# Constant that keeps the size of the grid (X x X)
const grid = 16
# Gets the world 2d
onready var world = get_world_2d().get_direct_space_state()
# Gets the player's node
onready var sprite = get_node("Sprite")
# Gets the player's animation
onready var animation = get_node("Animation")

func _ready():
	set_fixed_process(true)
	set_process_input(true)

func _input(event):
	if event.is_action_pressed("ui_interact"):
		interact = true
	elif event.is_action_released("ui_interact"):
		interact = false
	if event.is_action_pressed("ui_menu"):
		menu = true
	elif event.is_action_released("ui_menu"):
		menu = false

func _fixed_process(delta):
	# If player is not moving
	if !moving and can_move:
		#gets the intersect view point "up"
		var result_up = world.intersect_point(get_pos() + Vector2(0, -grid))
		#gets the intersect view point "down"
		var result_down = world.intersect_point(get_pos() + Vector2(0, grid))
		#gets the intersect view point "left"
		var result_left = world.intersect_point(get_pos() + Vector2(-grid, 0))
		#gets the intersect view point "right"
		var result_right = world.intersect_point(get_pos() + Vector2(grid, 0))

		# And the up arrow is pressed
		if Input.is_action_pressed("ui_up"):
			sprite.set_frame(10)
			if result_up.empty():
				# Sets moving to true
				moving = true
				# And vector to move the character to up
				direction = Vector2(0, -1)
				# And keeps the start movement position
				start_position = get_pos()
				# Play the animation
				animation.play("walk_up")
		elif Input.is_action_pressed("ui_down"):
			sprite.set_frame(1)
			if result_down.empty():
				# Sets moving to true
				moving = true
				# And vector to move the character to down
				direction = Vector2(0, 1)
				# And keeps the start movement position
				start_position = get_pos()
				# Play the animation
				animation.play("walk_down")
		elif Input.is_action_pressed("ui_left"):
			sprite.set_frame(4)
			if result_left.empty():
				# Sets moving to true
				moving = true
				# And vector to move the character to left
				direction = Vector2(-1, 0)
				# And keeps the start movement position
				start_position = get_pos()
				# Play the animation
				animation.play("walk_left")
		elif Input.is_action_pressed("ui_right"):
			sprite.set_frame(7)
			if result_right.empty():
				# Sets moving to true
				moving = true
				# And vector to move the character to up
				direction = Vector2(1, 0)
				# And keeps the start movement position
				start_position = get_pos()
				# Play the animation
				animation.play("walk_right")
		
		if interact:
			if sprite.get_frame() == 10:
				interact(result_up)
			elif sprite.get_frame() == 1:
				interact(result_down)
			elif sprite.get_frame() == 4:
				interact(result_left)
			elif sprite.get_frame() == 7:
				interact(result_right)

		if menu and !interact:
			get_node("Camera/Menu")._open_menu()

	elif can_move:
		move_to(get_pos() + direction * movement_speed)
		if get_pos() == start_position + Vector2(grid * direction.x, grid * direction.y):
			moving = false
	interact = false
	menu = false

func interact(result):
	for dictionary in result:
		if typeof(dictionary.collider) == TYPE_OBJECT and dictionary.collider.has_node("Interact"):
			get_node("Camera/Dialogue").set_hidden(false)
			get_node("Camera/Dialogue")._print_dialogue(dictionary.collider.get_node("Interact").text_to_print)