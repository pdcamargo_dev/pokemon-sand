extends KinematicBody2D
var world
# Constant that says the size of the grid (16x16)
const GRID = 16
# Variable that will be used to keep the start position of the player when he moves
var start_position = Vector2(0,0)
# Variable that says if player is moving
var moving = false
#Variable that checks if player can move
var can_move = true
#Variable that sets the stand motion speed (absolute, only changes if is permanent)
export var base_speed = 140
#Variabel that controls velocity of players movement
var motion_speed = base_speed
#Variabel that controls idle speed
export var idle_speed = 10
#Variable that says if player is running or walking
export var is_running = false
#Variable that keeps the speed bonus when the player is running
export var running_bonus = 60
#Variable that set the player's speed limit when running
var speed_run_limit = motion_speed + running_bonus
#Variable that says if player is on the bike
var on_bike = false
#Variable that keeps the speed bonus when the player is on the bike
export var bike_speed_bonus = 60
#Variable that keeps player's animation
onready var player_animation = get_node("Animation")
#Variables used after to better use the animations
var animation
var new_animation
#Variable that keeps the "sight" of the player to better use the controls and animation
onready var sight = get_node("Sight")
#Variable to move the player
var motion = Vector2(0,0)
#Variable that says if player is on a encounter area
var encounter_area = false
#Variable that says if a new encounter can be checked
var can_check_encounter = false
#Variable that says if menu is open or closed
var menu
#Variable that says if player is interacting
var interact = false
#Common variable to be used in loops
var i = 0

func _ready():
	world = get_world_2d().get_direct_space_state()
	#Allow fixed process to be used
	set_fixed_process(true)
	#Allow input process to be used
	set_process_input(true)

func _input(event):
	
	if event.is_action_pressed("ui_interact"):
		interact = true
	elif event.is_action_released("ui_interact"):
		interact = false
	
	if on_bike:
		speed_run_limit = motion_speed + running_bonus + bike_speed_bonus
	else:
		speed_run_limit = motion_speed + running_bonus
	
	if event.is_action_pressed("ui_menu"):
		if menu:
			menu = false
		else:
			menu = true
	
	# Set player to run on click of "Q" on keyboard
	if event.is_action_pressed("keyboard_q"):
		is_running = true
	elif event.is_action_released("keyboard_q"):
		is_running = false
	
	# Put player on bike
	if event.is_action_pressed("keyboard_r"):
		if not on_bike:
			on_bike = true
		else:
			on_bike = false
	
	if is_running:
		if not on_bike:
			motion_speed += running_bonus
		else:
			motion_speed += running_bonus + bike_speed_bonus

		if motion_speed > speed_run_limit:
			motion_speed = speed_run_limit

	else:
		if not on_bike:
			motion_speed = base_speed
		else:
			motion_speed = base_speed + bike_speed_bonus
	
	if menu:
		get_node("Camera/Menu")._open_menu()

func _fixed_process(delta):
	if !moving and can_move:

		var result_up = world.intersect_point(get_pos() + Vector2(0, -GRID))
		var result_down = world.intersect_point(get_pos() + Vector2(0, GRID))
		var result_left = world.intersect_point(get_pos() + Vector2(-GRID, 0))
		var result_right = world.intersect_point(get_pos() + Vector2(GRID, 0))

		# if the up arrow is tight, move the player up
		if Input.is_action_pressed("ui_up") and can_move:
			if result_up.empty():
				# Changes moving to true
				moving = true
				# Moves the player
				motion = Vector2(0, -1)
				# Changes the variable of start position to the current position
				start_position = get_pos()
				# Rotates the sight
				sight.set_rotd(180)
				if on_bike:
					animation = "WalkingBikeBack"
				else:
					animation = "WalkingBack"
				# Checks if player is moving and he is on a encounter area, and the encounter can be checked
				#if is_moving and encounter_area and can_check_encounter:
					#Controller.check_encounter()

		# if the down arrow is tight, move the player down
		if Input.is_action_pressed("ui_down") and can_move:
			if result_down.empty():
				# Changes moving to true
				moving = true
				# Moves the player
				motion = Vector2(0, 1)
				# Changes the variable of start position to the current position
				start_position = get_pos()
				# Rotates the sight
				sight.set_rotd(0)
				if on_bike:
					animation = "WalkingBikeFront"
				else:
					animation = "WalkingFront"
				# Checks if player is moving and he is on a encounter area, and the encounter can be checked
				#if is_moving and encounter_area and can_check_encounter:
					#Controller.check_encounter()

		# if the left arrow is tight, move the player to left
		if Input.is_action_pressed("ui_left") and can_move:
			if result_left.empty():
				# Changes moving to true
				moving = true
				# Flips the player horizontally
				player_animation.set_flip_h(false)
				# Moves the player
				motion = Vector2(-1, 0)
				# Changes the variable of start position to the current position
				start_position = get_pos()
				# Rotates the sight
				sight.set_rotd(-90)
				# Checks if player is moving and he is on a encounter area, and the encounter can be checked
				#if is_moving and encounter_area and can_check_encounter:
					#Controller.check_encounter()

		# if the right arrow is tight, move the player to right
		if Input.is_action_pressed("ui_right") and can_move:
			if result_right.empty():
				# Changes moving to true
				moving = true
				# Flips the player horizontally
				player_animation.set_flip_h(true)
				# Moves the player
				motion = Vector2(1, 0)
				# Changes the variable of start position to the current position
				start_position = get_pos()
				# Rotates the sight
				sight.set_rotd(90)
				if on_bike:
					animation = "WalkingBikeHorizontal"
				else:
					animation = "WalkingHorizontal"
				# Checks if player is moving and he is on a encounter area, and the encounter can be checked
				#if is_moving and encounter_area and can_check_encounter:
					#Controller.check_encounter()

		if interact:
			if sight.get_rotd() == 0:
				interact(result_down)
			elif sight.get_rotd() == 180:
				interact(result_up)
			elif sight.get_rotd() == -90:
				interact(result_left)
			elif sight.get_rotd() == 90:
				interact(result_right)

	elif can_move:
		if motion.length() <= idle_speed * 0.09:
			if sight.get_rotd() == 180:
				if on_bike:
					animation = "IdleBikeBack"
				else:
					animation = "IdleBack"
			if sight.get_rotd() == 0:
				if on_bike:
					animation = "IdleBikeFront"
				else:
					animation = "IdleFront"
			if sight.get_rotd() == 90 or sight.get_rotd() == -90:
				if on_bike:
					animation = "IdleBikeHorizontal"
				else:
					animation = "IdleHorizontal"

		if animation != new_animation:
			new_animation = animation
			player_animation.play(animation)

		move_to(get_pos() + motion * 2)

		if get_pos() == start_position + Vector2(GRID * motion.x, GRID * motion.y):
			moving = false

	interact = false
	menu = false

func interact(result):
	for dictionary in result:
		if typeof(dictionary.collider) == TYPE_OBJECT and dictionary.collider.has_node("Interact"):
			get_node("Camera/Dialogue").set_hidden(false)
			get_node("Camera/Dialogue")._print_dialogue(dictionary.collider.get_node("Interact").text_to_print)